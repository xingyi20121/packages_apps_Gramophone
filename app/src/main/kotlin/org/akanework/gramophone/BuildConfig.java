/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.akanework.gramophone;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "org.akanework.gramophone";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 3;
  public static final String VERSION_NAME = "1.0.2.88938a";
  // Field from default config.
  public static final String RELEASE_TYPE = "release";
}
